package pl.uj.hellospring02.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.uj.hellospring02.model.Book;
import pl.uj.hellospring02.service.BookService;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping(path = "/books")
public class HelloController {

    private BookService bookService;

    public HelloController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public List<Book> getBooks(@RequestParam(value = "upstream", required = false, defaultValue = "false") boolean upstream) {
        return bookService.getBooks(upstream);
    }
}
