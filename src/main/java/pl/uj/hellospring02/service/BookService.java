package pl.uj.hellospring02.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.uj.hellospring02.model.Book;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class BookService {
    @Autowired
    RestTemplate restTemplate;

    List<Book> bookRepository = new ArrayList<>();

    public BookService() {
        bookRepository.addAll(Arrays.asList(new Book(1, "Tomasz Dzialowy", "Zdolny do wszystkiego"),
                new Book(2, "Jordan B. Peterson", "12 zasad na zycie"),
                new Book(3, "Lech Walesa", "Moja droga z elektryka do prezydenta"))
        );
    }
    public List<Book> getBooks(boolean upstream) {
        if(upstream)
            return Stream.concat(bookRepository.stream(), getUpstreamBooks().stream()).collect(Collectors.toList());

        return bookRepository;
    }

    private List<Book> getUpstreamBooks() {
        ResponseEntity<Book[]> response = restTemplate.getForEntity("https://jwzp-mari339.herokuapp.com/books", Book[].class);
        return Arrays.asList(response.getBody());
    }
}
